iterations = 39000-2000;
A = 1.15;
C = 0.8;
m = 120;
g = 9.81;
rho0 = 1.25;
dx = 1;
x = 0;
y = 0;
xrecord = [];
yprecord = [];
yrecord = [];
xp = 0;

for i = 1:iterations,
    rho = rho0*e^(-0.000139*(39E3-x));
    yp = -A*C/m*rho*y+2*g;
    y = y + yp;
    x = x + dx;
    xrecord(end+1) = x;
    yprecord(end+1) = yp;
    yrecord(end+1) = y;
endfor

x = 0:39000;
k = 0.000139;
c1 = A*C/m * rho0;
c2 = 2*g/k*(-expint(-c1/k*exp(-k*(39000-0)))-i*pi);
%y = (2*g/k*expint(c1/k*exp(-k*(39000-x))) - c2) .* exp(-c1/k*exp(-k*(39000-x)));
%The two definitions are related, for positive real values of X, by
    %'E_1 (-x) = -Ei (x) - i*pi: Ei (x) = -E_1 (-x) - i*pi'.
y = (2*g/k*(-expint(-c1/k*exp(-k*(39000-x)))-i*pi) - c2) .* exp(-c1/k*exp(-k*(39000-x)));

%hold off
%plot(xrecord,yprecord*10000)
%hold on
%plot(xrecord,yrecord)
plot(x,y)
grid on
%legend("yprecord","y")
legend("y")
